from setuptools import setup, find_packages
import configparser

# Get config parameters
config = configparser.ConfigParser()
config.read('setup.cfg')
pkg_name = config['metadata']['name']


setup(
    python_requires='>=3.5',
    packages=find_packages(),  # if offering a package
    # py_modules=['pkgname.replace('-', '_')'],  # if offering a single module file
)
