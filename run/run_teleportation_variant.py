"""
Copyright 2020 Julius Wallnöfer

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
"""

from __future__ import division, print_function
from argparse import ArgumentParser
import os, sys; sys.path.insert(0, os.path.abspath("."))
from time import time
# from environments.teleportation_variant_1 import TaskEnvironment as TeleportationVariantEnv
from environments.teleportation_variant_2 import TaskEnvironment as TeleportationVariantEnv
from agents.ps_agent_changing_actions import ChangingActionsPSAgent
from general_interaction import Interaction
from multiprocessing import Pool
import numpy as np
import os
import matplotlib.pyplot as plt


def run_teleportation(i, eta, label_multiplicator=10, sparsity=20):
    np.random.seed()
    env = TeleportationVariantEnv()
    agent = ChangingActionsPSAgent(env.n_actions, ps_gamma=0, ps_eta=eta, policy_type="softmax", ps_alpha=1, brain_type="dense", reset_glow=True)
    interaction = Interaction(agent=agent, environment=env)
    res = interaction.single_learning_life(n_trials=args.n_trials, max_steps_per_trial=50)
    learning_curve = res["learning_curve"]
    success_list = np.ones(len(learning_curve), dtype=np.int)
    success_list[learning_curve == 0] = 1
    learning_curve[learning_curve == 0] = 10**-4
    step_curve = learning_curve**-1
    if sparsity != 1:
        step_curve = step_curve[0::sparsity]
        success_list = success_list[0::sparsity]
    # np.savetxt("results/eta_%d/step_curve_%d.txt" % (eta * label_multiplicator, i), step_curve, fmt="%.5f")
    np.save(result_path + "eta_%d/step_curve_%d.npy" % (eta * label_multiplicator, i), step_curve)
    np.savetxt(result_path + "eta_%d/success_list_%d.txt" % (eta * label_multiplicator, i), success_list, fmt="%-d")


def get_label_multiplicator(eta):
    return 10**(len(str(eta)) - 2)


class RunCallable(object):  # this solution is necessary because only top-level objects can be pickled
    def __init__(self, eta):
        self.eta = eta

    def __call__(self, i):
        return run_teleportation(i,
                                 self.eta,
                                 label_multiplicator=get_label_multiplicator(self.eta),
                                 sparsity=args.sparsity)


# def callback_error(result):
#     print('error', result)


if __name__ == "__main__":
    start_time = time()

    parser = ArgumentParser()
    parser.add_argument('--n_processes', required=False, default=64, type=int,
                        help="Change according to number of cores in cluster. Defaults to 64.")
    parser.add_argument('--n_agents', required=False, default=500, type=int,
                        help="Number of agents for which to run learning procedure. Defaults to 500.")
    parser.add_argument('--etas', required=False, nargs='+',
                        default=[0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7], type=int,
                        help="Different values of glow parameter to consider.")
    parser.add_argument('--n_trials', required=False, default=10**6, type=int,
                        help="Number of trials for which to run learning process. Defaults to 10^6.")
    parser.add_argument('--result_path', required=False, default="results/teleportation/variant2/raw/", type=str,
                        help="Path to store results. Defaults to results/teleportation/variant2/raw/.")
    parser.add_argument('--sparsity', required=False, default=50, type=int,
                        help="Defaults to 50.")

    args = parser.parse_args()
    p = Pool(processes=args.n_processes)
    for eta in args.etas:
        if not os.path.exists(args.result_path + "eta_%d/" % (eta * get_label_multiplicator(eta))):
            os.makedirs(args.result_path + "eta_%d/" % (eta * get_label_multiplicator(eta)))
        my_callable = RunCallable(eta)
        p.map_async(my_callable, np.arange(args.n_agents))
    p.close()
    p.join()
    print("The whole script took %.2f minutes." % ((time() - start_time) / 60))
