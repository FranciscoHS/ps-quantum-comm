"""
Copyright 2020 Julius Wallnöfer

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
"""

from __future__ import division, print_function
from argparse import ArgumentParser
import os, sys; sys.path.insert(0, os.path.abspath("."))
from environments.epp_env import EPPEnv
from meta_analysis_interaction import MetaAnalysisInteraction
from agents.ps_agent_changing_actions import ChangingActionsPSAgent
from multiprocessing import Pool
from time import time
import numpy as np
import os
import traceback


def assert_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)


def run_epp(i, sparsity=10):
    np.random.seed()
    env = EPPEnv()
    agent = ChangingActionsPSAgent(env.n_actions, ps_gamma=0, ps_eta=args.eta, policy_type="softmax", ps_alpha=1, brain_type="dense")  # glow reset is handled by MetaAnalysis Interaction
    interaction = MetaAnalysisInteraction(agent, env)
    last_history_file = args.result_path + "last_trial_history_%d.txt" % i
    res = interaction.single_learning_life(args.n_trials, verbose_trial_count=False, last_history_file=last_history_file, depolarize=True)
    reward_curve = res["reward_curve"]
    if sparsity != 1:
        reward_curve = reward_curve[::sparsity]
    np.save(args.result_path + "reward_curve_%d.npy" % i, reward_curve)
    print(str(i) + "; n_percepts: " + str(len(agent.percept_dict)))


class RunCallable(object):  # this solution is necessary because only top-level objects can be pickled
    def __init__(self):
        pass

    def __call__(self, i):
        try:
            return run_epp(i)
        except Exception:
            print("Exception occured in child process")
            traceback.print_exc()  # so exception in child process gets output on older python versions
            raise


if __name__ == "__main__":
    start_time = time()

    parser = ArgumentParser()
    parser.add_argument('--n_processes', required=False, default=64, type=int,
                        help="Change according to number of cores in cluster. Defaults to 64.")
    parser.add_argument('--n_agents', required=False, default=100, type=int,
                        help="Number of agents for which to run learning procedure. Defaults to 100.")
    parser.add_argument('--eta', required=False, default=0, type=int, help="Glow parameter.")
    parser.add_argument('--n_trials', required=False, default=500000, type=int,
                        help="Number of trials for which to run learning process. Defaults to 500000.")
    parser.add_argument('--result_path', required=False, default="results/epp_modified_depolarized/raw/", type=str,
                        help="Path to store results. Defaults to results/epp_modified_depolarized/raw/.")

    args = parser.parse_args()
    p = Pool(processes=args.n_processes)
    assert_dir(args.result_path)
    my_callable = RunCallable()
    p.map_async(my_callable, np.arange(args.n_agents))
    p.close()
    p.join()
    print("The whole script took %.2f minutes." % ((time() - start_time) / 60))
